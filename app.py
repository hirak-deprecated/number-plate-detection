from flask import Flask, request, redirect, url_for
from flask_restful import Resource, Api, reqparse
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "images"


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/uploadNumberPlate', methods=['POST'])
def receive_number_plate():
    if 'image' in request.files:
        license_plate = request.files['image']
        file_name = secure_filename(license_plate.filename)
        license_plate.save(os.path.join(app.config['UPLOAD_FOLDER'], file_name))
        return file_name
    else:
        return 'No file'


if __name__ == '__main__':
    app.run(debug=True)
